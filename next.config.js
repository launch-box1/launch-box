/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["launchbox.pk"],
  },
};

module.exports = nextConfig;
