import Navbar from "../components/Navbar/index";
import Footer from "../components/Footer/Footer";
import Banner from "../components/Banner/Banner.jsx";
import Buyers from "../components/Buyers/index";
import Provide from "../components/Provide/index";
import Why from "../components/Why/index";
import Network from "../components/Network/index";
import Clientsay from "../components/Clientsay/index";
import About from "../components/About/index";
import Contact from "../components/Contact/index";

export const metadata = {
  title: "LaunchBox",
};

export default function Home() {
  return (
    <main>
      <Navbar />
      <Banner />
      <Buyers />
      <Provide />
      <About />
      <Network />
      <Why />
      <Clientsay />
      <Contact />
      <Footer />
    </main>
  );
}
