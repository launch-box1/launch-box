import Link from "next/link";
import Image from "next/image";

const products = [
  {
    id: 1,
    link: ["Home", "About"],
  },
  {
    id: 2,
    link: ["Services", "Contact"],
  },
];

const footer = () => {
  return (
    <div className="mt-12">
      <div className="bg-[#33D1B5] rounded-tl-[40px] rounded-tr-[40px] h-8" />
      <div className="mx-auto max-w-2xl pt-4 px-4 sm:px-6 lg:max-w-7xl lg:px-8">
        <div className="my-10 grid grid-cols-1 gap-y-10 gap-x-16 sm:grid-cols-2 lg:grid-cols-12 xl:gap-x-8">
          {/* COLUMN-1 */}
          <div className="col-span-4 md:col-span-12 lg:col-span-4 min-[360px]:flex min-[360px]:place-content-center">
            <div>
              <img
                src={
                  "https://launchbox.pk/wp-content/uploads/2023/04/LB-Logo.png"
                }
                alt="logo"
                className="pb-8 w-64"
              />
              <div className="flex gap-4 min-[360px]:place-content-center">
                <Link href="https://facebook.com" className="footer-fb-icons">
                  <Image
                    src={"/assets/footer/facebook.svg"}
                    alt="facebook"
                    width={15}
                    height={20}
                  />
                </Link>
                <Link href="https://twitter.com" className="footer-icons">
                  <Image
                    src={"/assets/footer/twitter.svg"}
                    alt="twitter"
                    width={20}
                    height={20}
                  />
                </Link>
                <Link href="https://instagram.com" className="footer-icons">
                  <Image
                    src={"/assets/footer/instagram.svg"}
                    alt="instagram"
                    width={20}
                    height={20}
                  />
                </Link>
              </div>
            </div>
          </div>

          {/* CLOUMN-2/3 */}

          {products.map((product) => (
            <div
              key={product.id}
              className="group relative mt-10 col-span-2 md:col-span-4 lg:col-span-2 min-[360px]:ml-12 min-[360px]:mr-12"
            >
              <ul>
                {product.link.map((link, index) => (
                  <li key={index} className="mb-5">
                    <Link
                      href="/"
                      className="text-black text-sm font-normal mb-6 space-links"
                    >
                      {link}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          ))}

          {/* CLOUMN-4 */}

          <div className="col-span-4 mt-6 md:col-span-4 lg:col-span-4 min-[360px]:w-68 min-[360px]:ml-12">
            <div className="flex gap-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="#33D1B5"
                className="w-6 h-6 -ml-2 min-[360px]:h-8 min-[360px]:w-9"
              >
                <path
                  fillRule="evenodd"
                  d="M11.54 22.351l.07.04.028.016a.76.76 0 00.723 0l.028-.015.071-.041a16.975 16.975 0 001.144-.742 19.58 19.58 0 002.683-2.282c1.944-1.99 3.963-4.98 3.963-8.827a8.25 8.25 0 00-16.5 0c0 3.846 2.02 6.837 3.963 8.827a19.58 19.58 0 002.682 2.282 16.975 16.975 0 001.145.742zM12 13.5a3 3 0 100-6 3 3 0 000 6z"
                  clipRule="evenodd"
                />
              </svg>

              <h5 className="text-base font-normal text-black -mt-4 max-[450px]:text-[15px]">
                Suite No. 906, 9th Floor, Mehdi Tower <br />
                SMCHS, Main Shahrah-e-Faisal, <br />
                Karachi - 75100
              </h5>
            </div>
            <div className="flex gap-2 mt-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="#33D1B5"
                className="w-6 h-6"
              >
                <path
                  fillRule="evenodd"
                  d="M1.5 4.5a3 3 0 013-3h1.372c.86 0 1.61.586 1.819 1.42l1.105 4.423a1.875 1.875 0 01-.694 1.955l-1.293.97c-.135.101-.164.249-.126.352a11.285 11.285 0 006.697 6.697c.103.038.25.009.352-.126l.97-1.293a1.875 1.875 0 011.955-.694l4.423 1.105c.834.209 1.42.959 1.42 1.82V19.5a3 3 0 01-3 3h-2.25C8.552 22.5 1.5 15.448 1.5 6.75V4.5z"
                  clipRule="evenodd"
                />
              </svg>

              <h5 className="text-base font-normal text-black">
                + 45 34 11 44 11
              </h5>
            </div>
            <div className="flex gap-2 mt-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="#33D1B5"
                className="w-6 h-6"
              >
                <path d="M1.5 8.67v8.58a3 3 0 003 3h15a3 3 0 003-3V8.67l-8.928 5.493a3 3 0 01-3.144 0L1.5 8.67z" />
                <path d="M22.5 6.908V6.75a3 3 0 00-3-3h-15a3 3 0 00-3 3v.158l9.714 5.978a1.5 1.5 0 001.572 0L22.5 6.908z" />
              </svg>

              <h5 className="text-base font-normal text-black">
                info@gmail.com
              </h5>
            </div>
          </div>
        </div>

        {/* All Rights Reserved */}

        <div className="py-10 justify-between border-t border-t-bordertop">
          <h4 className="text-textblack text-sm text-center font-normal">
            @2023 Agency. All Rights Reserved by{" "}
            <Link href="https://adminmart.com/" target="_blank">
              {" "}
              Adminmart.com
            </Link>
          </h4>
        </div>
      </div>
      <div className="bg-[#33D1B5] rounded-bl-[40px] rounded-br-[40px] h-8 mb-2" />
    </div>
  );
};

export default footer;
