import Image from "next/image";
import Link from "next/link";

const Aboutdata = [
  {
    imgSrc: "/assets/provide/marketing.svg",
    country: "App Development",
    paragraph: "Innovate your ideas with app development",
  },
  {
    imgSrc: "/assets/provide/graphic.svg",
    country: "Web Development",
    paragraph: "Unleash your digital presence with web development",
  },
  {
    imgSrc: "/assets/provide/heaking.svg",
    country: "Digital Marketing",
    paragraph: "Amplify your brand with digital marketing",
  },
  {
    imgSrc: "/assets/provide/uidesign.svg",
    country: "Video Animation",
    paragraph: "Bring your story to life with video animation",
  },
  {
    imgSrc: "/assets/provide/heaking.svg",
    country: "Prototyping",
    paragraph: "Prototype your ideas for seamless execution",
  },
  {
    imgSrc: "/assets/provide/uidesign.svg",
    country: "Business Solutions",
    paragraph: "Empower your business with tailored solutions",
  },
];

const Provide = () => {
  return (
    <div id="services" className="ml-12 max-[450px]:ml-2">
      <div className="mx-auto max-w-8xl px-4 my-10 max-[450px]:my-0 max-[450px]:px-0 lg:px-8 max-[450px]:w-full">
        <div className="grid grid-cols-1 lg:grid-cols-12 gap-8 max-[450px]:gap-2">
          {/* COLUMN-1 */}
          <div className="col-span-6 flex justify-center ml-20 max-[450px]:ml-0">
            <div className="flex flex-col align-middle justify-center p-10 max-[450px]:p-8">
              <div className="pl-16 pr-16 max-[450px]:pr-2 max-[450px]:pl-2">
                <p className="text-4xl lg:text-6xl pt-4 font-semibold lh-81 mt-5 max-[450px]:mt-1 max-[450px]:text-3xl text-center lg:text-start">
                  We provide that service.
                </p>
                <h4 className="text-lg pt-4 font-normal lh-33 text-center lg:text-start text-bluegray">
                  With our expertise in cutting-edge technologies, we deliver
                  innovative solutions that enhance user experiences, optimize
                  conversions, and position your business at the forefront of
                  the digital landscape.
                </h4>
              </div>
            </div>
          </div>
          {/* COLUMN-2 */}
            <div className="col-span-6 lg:col-span-5 -ml-12 max-[450px]:-ml-2">
              <div className="grid grid-cols-1 sm:grid-cols-3 gap-x-16 gap-y-10 lg:gap-x-40 px-10 py-12 bg-[#00F5C8] rounded-3xl">
                {Aboutdata.map((item, i) => (
                  <div
                    key={i}
                    className="bg-white rounded-3xl lg:-ml-32 p-6 shadow-xl"
                  >
                    <Image
                      src={item.imgSrc}
                      alt={item.imgSrc}
                      width={64}
                      height={64}
                      className="mb-5"
                    />
                    <h4 className="text-xl font-semibold">{item.country}</h4>
                    <h4 className="text-lg font-normal text-bluegray my-2">
                      {item.paragraph}
                    </h4>
                  </div>
                ))}
              </div>
            </div>
        </div>
      </div>
    </div>
  );
};

export default Provide;
