const HeroSection = () => {
  return (
    <section className="hero-section">
      <div
        className="bg-image bg-cover h-screen bg-center relative -ml-1"
        style={{ backgroundImage: "url(/assets/bgimage1.png)" }}
      >
        <div className="bg-opacity-50 absolute inset-0"></div>
        <div className="container mx-auto px-6 lg:px-8 py-20 lg:py-32">
          <h1 className="text-4xl text-center mt-48 font-semibold text-white sm:text-5xl lg:text-7xl md:4px lh-96">
            Transform Your Ideas into <br /> Powerful Solutions
          </h1>
          <p className="mt-6 text-lg text-center leading-8 text-white">
            We are your secret weapon and your partner in success as we turn
            your aspirations into reality,
            <br /> pixel by pixel, line by line
          </p>
          <div className="flex place-content-center mt-4">
            <button className="text-[20px] w-64 tracking-widest font-semibold border-2 rounded-full mt-6 relative z-10 text-[#9BE8DA] border-[#9BE8DA] py-3 mt-2 hover:bg-[#9BE8DA] hover:text-black">
              See How
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeroSection;
